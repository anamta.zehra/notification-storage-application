package com.decipher.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.decipher.dao.UserRepository;
import com.decipher.request.UserRequest;
import com.decipher.response.UserResponse;
import com.decipher.services.UserServices;


@RestController
@RequestMapping("api/users")
public class UserController {

	@Autowired
	UserServices userService;

	@Autowired
	UserRepository userRepository;

	@PostMapping("api/users/create")
	ResponseEntity<String> SaveUser(@RequestBody UserRequest user) {
		System.out.println(userService.checkEmailExistOrNot(user.getEmail()));
		if (userService.checkEmailExistOrNot(user.getEmail())) {
			return new ResponseEntity<>("User Created Successfully", HttpStatus.OK);
		}
		userService.saveUserData(user);
		return new ResponseEntity<>("This User Account Is Already exist in DataBase Try With Another Email", HttpStatus.OK);
	}

	@GetMapping("api/users/show")
	List<UserResponse> getUsersData() {
		List<UserResponse> list = new ArrayList<UserResponse>();
		list.addAll(userService.getAlllUsers());
		System.out.println(list);
		return list;
	}

	@PutMapping("api/users/update")
	ResponseEntity<String> updateUser(@RequestBody UserRequest user) {
		System.out.println(userService.checkEmailExistOrNot(user.getEmail()));
		if (userService.checkEmailExistOrNot(user.getEmail())) {
			int updateRecordCount = userService.updateUserRecord(user);
			return new ResponseEntity<>(updateRecordCount + "Record is updated", HttpStatus.OK);
		}
		return new ResponseEntity<>("this User Record is not Exist in The DataBase", HttpStatus.OK);
	}


	@DeleteMapping("api/users/delete")
	ResponseEntity<String> deleteUser(@RequestBody UserRequest user) {
		System.out.println(userService.checkEmailExistOrNot(user.getEmail()));
		if (userService.checkEmailExistOrNot(user.getEmail())) {
			int deleteRecordCount = userService.deleteByEmail(user.getEmail());
			//userService.saveUserData(user);
			return new ResponseEntity<>(HttpStatus.OK);  //This user record exist in Database.
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND); //Not exist
	}

	@GetMapping("api/users/notificationsSizedUser")
	List<Map<String, Object>> userRecordById(@PathVariable int userId) {
		return userService.userwithNotification(userId);
	}


}