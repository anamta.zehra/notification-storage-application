package com.decipher.request;

import lombok.Data;
@Data
public  class UserRequest {

	private String name;
	private String email;
	private String phoneNo;

}
