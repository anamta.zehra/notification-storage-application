package com.decipher.entities;

import lombok.Data;

@Data

public class UserEntity
{
    public UserEntity(){

    }
	private int id;
	private String name;
	private String email;
	private String phoneNo;

    public UserEntity(int id, String name, String email, String phoneNo) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNo = phoneNo;
    }
}




