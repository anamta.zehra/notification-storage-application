package com.decipher.entities;

import lombok.Data;
@Data
public class NotificationEntity {

	public NotificationEntity(){

	}

	private String notificationType;
	private int user;
	private String notifyTime;
	private String dateCreated;
	private String lastUpdated;
	private boolean enabled;
	private boolean isSent;
	private boolean isRepeat;

	public NotificationEntity(String notificationType, int user, String notifyTime, String dateCreated, String lastUpdated, boolean enabled, boolean isSent, boolean isRepeat) {
		this.notificationType = notificationType;
		this.user = user;
		this.notifyTime = notifyTime;
		this.dateCreated = dateCreated;
		this.lastUpdated = lastUpdated;
		this.enabled = enabled;
		this.isSent = isSent;
		this.isRepeat = isRepeat;
	}
}

