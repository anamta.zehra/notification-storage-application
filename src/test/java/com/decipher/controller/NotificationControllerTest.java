package com.decipher.controller;

import com.decipher.entities.NotificationEntity;
import com.decipher.response.NotificationResponse;
import com.decipher.response.UserResponse;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class NotificationControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getNotificationTest()  {

        ResponseEntity<List<NotificationResponse>> exchange = restTemplate.exchange(
                "/api/users/api/users/showNotification",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<NotificationResponse>>() {
                }
        );
        Assert.assertEquals(HttpStatus.OK,exchange.getStatusCode());
    }
    @Test
    public void saveNotificationTest() {
        NotificationEntity notification = new NotificationEntity("SMS",4,"2019-01-09 15:48:23","2019-01-09 15:48:23","2019-01-09 15:48:23",false,true,true);
        HttpEntity<NotificationEntity> entity = new HttpEntity<>(notification);
        ResponseEntity<String> exchange = restTemplate.exchange(
                "/api/users/api/users/saveNotification",
                HttpMethod.POST,
                entity,
                String.class);
        Assert.assertEquals(HttpStatus.OK,exchange.getStatusCode());
    }
    @Test
    public void deleteNotificationTest(){
        NotificationEntity notification = new NotificationEntity("SMS",4,"2019-01-09 15:48:23","2019-01-09 15:48:23","2019-01-09 15:48:23",false,false,false);
        HttpEntity<NotificationEntity> entity = new HttpEntity<>(notification);
        ResponseEntity<String> exchange = restTemplate.exchange(
                "/api/users/api/users/deleteNotification",
                HttpMethod.DELETE,
                entity,
                String.class);
        Assert.assertEquals(HttpStatus.OK,exchange.getStatusCode());
    }
    @Test
    public void updateNotificationTest()  {
        NotificationEntity notification = new NotificationEntity("EMail",7,"2019-01-09 15:48:23","2019-01-09 15:48:23","2019-01-09 15:48:23",true,true,true);
        HttpEntity<NotificationEntity> entity = new HttpEntity<>(notification);
        ResponseEntity<String> exchange = restTemplate.exchange(
                "/api/users/api/users/updateNotification",
                HttpMethod.PUT,
                entity,
                String.class);
        Assert.assertEquals(HttpStatus.OK,exchange.getStatusCode());
    }
}