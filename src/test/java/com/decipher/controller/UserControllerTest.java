package com.decipher.controller;

import com.decipher.entities.UserEntity;
import com.decipher.response.UserResponse;
import org.apache.catalina.User;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;

import java.util.Arrays;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getAllUserTest()  {

        ResponseEntity<List<UserResponse>> exchange = restTemplate.exchange(
                "/api/users/api/users/show",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<UserResponse>>() {
                }
        );
        Assert.assertEquals(HttpStatus.OK,exchange.getStatusCode());
    }
    @Test
    public void postMappingTest() {
        UserEntity user = new UserEntity(9,"raaz","vanshika@yahoo.com","9878675645");
        HttpEntity<UserEntity> entity = new HttpEntity<UserEntity>(user);
        ResponseEntity<String> exchange = restTemplate.exchange(
                "/api/users/api/users/create",
                HttpMethod.POST,
                entity,
                String.class);
        Assert.assertEquals(HttpStatus.OK,exchange.getStatusCode());
    }
    @Test
    public void deleteMappingTest(){
        UserEntity user = new UserEntity(1,"anamta","anamta@gmail.com","9129305943");
        HttpEntity<UserEntity> entity = new HttpEntity<>(user);
        ResponseEntity<String> exchange = restTemplate.exchange(
                "/api/users/api/users/delete",
                HttpMethod.DELETE,
                entity,
                String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND,exchange.getStatusCode());
    }

    @Test
    public void putAllUserTest()  {
        UserEntity user = new UserEntity(1,"Heer","anamta@gmail.com","9129305943");
        HttpEntity<UserEntity> entity = new HttpEntity<>(user);
        ResponseEntity<String> exchange = restTemplate.exchange(
                "/api/users/api/users/update",
                HttpMethod.PUT,
                entity,
                String.class);
        Assert.assertEquals(HttpStatus.OK,exchange.getStatusCode());
    }
}
